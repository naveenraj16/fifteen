$(document).ready(function() {
    var baseHtml = "http://tinman.cs.gsu.edu/~raj/2010/sp10/project/";

    //construct the initial board
    var imageList = ["1.gif","2.gif","3.gif","4.gif","5.gif","6.gif","7.gif","8.gif","9.gif","10.gif","11.gif","12.gif","13.gif","14.gif","15.gif","blank.gif"];
    var board = ["","","","","","","","","","","","","","","",""];
    var blankIndex = 15;
    var clicks = 0;

    for(var i = 0; i < 15; i++){
        var pos = Math.floor(Math.random()*15);
        while(board[pos] != "") {
            pos = Math.floor(Math.random() * 15);
        }
        board[pos] = imageList[i];
    }
    board[15] = imageList[15];


    //add to table
    for(var i = 0; i < 16; i++) {
        var htmlString = "<img src=\"" + baseHtml + "hideImages/" + board[i] + "\">"
        $("#"+i).html(htmlString);
    }

    //enable clicks

    $("#board td").on('click', function () {
        var col = parseInt( $(this).index() ) ;
        var row = parseInt( $(this).parent().index() );
        var index = 4*row + col;
        //clear array for next clicks

        var neighbors = $(this).getNeighbors(index);

        //if the clicked cell has the blank space as a neighbor then swap them.
        for(var i = 0; i < neighbors.length; i++) {
            if (neighbors[i] == blankIndex) {
                clicks++;
                var temp = board[blankIndex];
                board[blankIndex] = board[index];
                board[index] = temp;
                var t = blankIndex;
                blankIndex = index;
                index = t;
                var htmlString = "<img src=\"" + baseHtml + "hideImages/" + board[index] + "\">"
                $("#"+index).html(htmlString);
                htmlString = "<img src=\"" + baseHtml + "hideImages/" + board[blankIndex] + "\">"
                $("#"+blankIndex).html(htmlString);
                if($(this).checkWin(board)){
		   for(var j = 0; j < 16; j++) {
                     var htmlString = "<img src=\" http://tinman.cs.gsu.edu/~raj/2010/sp10/project/win.gif\">";
                     $("#"+j).html(htmlString);
                   }
                    $("#clickDiv").html("You took "+clicks+" clicks to finish.");
                }
                return;
            }
        }

    });

    $.fn.getNeighbors = function(index){
        var neighbors = [];
        if(index == 0){
            neighbors.push(1);
            neighbors.push(4);
        }
        else if(index == 1){
            neighbors.push(0);
            neighbors.push(2);
            neighbors.push(5);
        }
        else if(index == 2){
            neighbors.push(1);
            neighbors.push(3);
            neighbors.push(6);
        }
        else if(index == 3){
            neighbors.push(2);
            neighbors.push(7);
        }
        else if(index == 4){
            neighbors.push(0);
            neighbors.push(5);
            neighbors.push(8);
        }
        else if(index == 5){
            neighbors.push(1);
            neighbors.push(4);
            neighbors.push(6);
            neighbors.push(9);
        }
        else if(index == 6){
            neighbors.push(2);
            neighbors.push(5);
            neighbors.push(7);
            neighbors.push(10);
        }
        else if(index == 7){
            neighbors.push(3);
            neighbors.push(6);
            neighbors.push(11);
        }
        else if(index == 8){
            neighbors.push(4);
            neighbors.push(9);
            neighbors.push(12);
        }
        else if(index == 9){
            neighbors.push(5);
            neighbors.push(8);
            neighbors.push(10);
            neighbors.push(13);
        }
        else if(index == 10){
            neighbors.push(6);
            neighbors.push(9);
            neighbors.push(11);
            neighbors.push(14);
        }
        else if(index == 11){
            neighbors.push(7);
            neighbors.push(10);
            neighbors.push(15);
        }
        else if(index == 12){
            neighbors.push(8);
            neighbors.push(13);
        }
        else if(index == 13){
            neighbors.push(9);
            neighbors.push(12);
            neighbors.push(14);
        }
        else if(index == 14){
            neighbors.push(10);
            neighbors.push(13);
            neighbors.push(15);
        }
        else {//index=15
            neighbors.push(11);
            neighbors.push(14);
        }
        return neighbors;
    }

    $.fn.checkWin = function(board){
        for(var i = 0; i < board.length; i++)
            if(board[i] != imageList[i])
                return false;
        return true;
    }


});
